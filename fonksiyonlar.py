def hello(string):
    hello = "Hello " + string
    print(hello)


hello("Köfte")

def toplayip_carp(t1, t2, c1):
    toplam = t1 + t2
    carpim = toplam * c1
    return carpim

ilk_sonuc = toplayip_carp(4, 6, 2)

sonuc = toplayip_carp(ilk_sonuc, 10, 3)

print(sonuc)
print(sonuc - 30)
print(sonuc)

def default_deger(n1=2, n2=4, n3=None):
    print(n1 + n2 + n3)

default_deger(n3=10) # default_deger(n1=2, n2=4, n3=10) buna eşit
default_deger(n3=10, n1=4, n2=6)