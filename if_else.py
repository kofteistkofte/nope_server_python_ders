def iki_mi(deger):
    if deger == 2:
        print("Evet Abi")
    else:
        print("Saçmalama be abi")

iki_mi(1)

iki_mi(2)

a = "Hell"
b = "World"

if a == "World":
    print("Evet a Hellodur")
elif a.lower() == "hello":
    print("a hellodur ama büyük, küçük fark etmez")
else:
    print("hacı sen ne yaptın")


if 1 <= 1:
    print("1 2den küçük")

if 2 > 1:
    print("2 > 1")

if 2 >= 1:
    print("2 >= 1")

if 2 != 1:
    print("bunlar farklı sayı")

if 2 != 2:
    print("eeee")
else:
    print("hacı eşit değil")

a = 12
b = 4

if a == 12 and b == 5:
    print("ikisi de doğru")
elif a == 13 and b == 4:
    print("ikinci seçenek doğru çıktı")
elif a == 12 and b == 4:
    print("Üçüncü seçenek doğru çıktı")
else:
    print("Lütfen daha sonra tekrar deneyiniz")

if a == 14 or b == 4:
    print("birinden biri doğru")

if a == 12 or not b == 4:
    print("Ne oldu şimdi")


def topla(n1=2, n2=4, n3=None):
    if n3: # n3'ün bir değeri var mı yok mu diye kontrol ediyor
        print("n3 verildi")
        print(n1 + n2 + n3)
    else:
        print("n3 verilmedi")
        print(n1 + n2)

topla()

topla(n3=10)