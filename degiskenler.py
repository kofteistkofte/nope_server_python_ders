"""
DEGISKEN TIPLERI
"""
# string
string_degiskeni = 'Bu bir string.'

# integer (tam sayi)
integer_degiskeni = 42

# float (ondalikli sayi)
float_degiskeni = 42.10

# boolean (sadece iki degerden birini alabilir, True | False)
boolean_degiskeni = True
boolean_degiskeni = False

""" 
LIST 

List degiskeni degerleri sirali, yani listelenmis sekilde tutar.
Degiskenler index numaralarina gore siralanir, o numara 0, 1, 2....diye gider.
"""

print('\nLIST')

list_degiskeni = ['Yolo', string_degiskeni, integer_degiskeni]
print('\nButun array')
print(list_degiskeni)


print('\nList\'in 0 numrarali index\'inde bulunan eleman: ' +
      list_degiskeni[0])
print('List\'in 1 numrarali index\'inde bulunan eleman: ' + list_degiskeni[1])

"""
DICTIONARY

List'in icindeki spesifik bir degeri gormek isteyince onu index numarasi ile cagirmistik.
-> list_degiskeni[0]

Dictionary de list gibi birden fazla degeri icinde tutar ama onlara ulasmak icin 
index numarasi kullanmak yerine anahtar kelime kullanir. (Birazdan gorecegiz.)
"""

dict_degiskeni = {
    "isim": "Osman",
    "soyisim": "KIRPAT",
    "dogumYili": 1995,
    "sevdigiRenkler": [
        'mavi',
        'yesil'
    ]
}
print('\nDICTIONARY')
print("'isim' anahtar kelimesinin degeri: " + dict_degiskeni["isim"])

# dogum yili string degildi, onu string'e cevirdik
print("'dogumYili' anahtar kelimesinin degeri: " +
      str(dict_degiskeni["dogumYili"]))

print("\n'sevdigiRenkler' anahtar kelimesinin degeri: ")
print(dict_degiskeni["sevdigiRenkler"])