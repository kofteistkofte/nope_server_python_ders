print("Hello World") # "Hello World" Çıktısı verece

hello = "Hello" # "hello" adında bir değişken oluşturup içerisine "Hello" yazacak
world = "World" # "world" adında bir değişken oluşturup içerisine "World" yazacak

print(hello + world) # ekrana "HelloWorld" yazdıracak.
print(hello + " " + world) # ekrana "Hello World" yazdıracak.
print(hello, world) # Bu da ekrana "Hello World" yazdıracak

world = "Sekai" # "world" isimli değişkenimizin içeriğini "Sekai" olarak değiştirecek.

print(hello, world) # Ekrana "Hello Sekai" yazdıracak çünkü öncesinde world değişkeni değişti

hello_world = hello + " " + world 
# "hello" ve "world" değişkenlerini aralarına bir boşluk ekleyerek birleştirip "hello_world" ismiyle kaydediyor

print(hello_world) # Ekrana gene "Hello Sekai" yazdıracak.

hello += " World" # "hello" değişkenini alıp sonuna " World" ekliyor.

print(hello) # Ekrana "Hello World" yazdırıyor.
print(hello, world) # Ekrana "Hello World Sekai" yazdırıyor, çünkü öncesinde "world" değişkeni değişti.

number = 42 # "number" adında bir değişken oluşturup bunun değerini sayı olarak "42" atıyor. Bu bir int değişkenidir.

"""
print(hello + 42)
# Bu satır kesinlikle hata verecektir, çünkü Python string ve intlerin beraber işlem görmesini istemez.
"""
# Birden fazla satırı """ yani üç tane tırnak arasına alırsanız bütün bu satırlar comment olarak geçer, yani kod çalışırken es geçilir

print(hello, str(number)) # ekrana "Hello World 42" yazdırıyor. str fonksiyonu içindeki her şeyi string haline çevirir.

print("Hello \"World\"") # Ekrana "Hello "World"" yazdıracak. "\" özel karakterleri es geçmemizi sağlıyor.

print("Hello")
print("World")
# Bu efektif olmayan bir biçimde yazıyı birden fazla satır olarak yazmamızı sağlayacak.

print("Hello\nWorld") # "\n" ile "Hello" ve "World" arasında satır boşluğu oluşturulur.