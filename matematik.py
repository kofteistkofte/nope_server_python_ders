2 + 2 # 2 ile 2'yi toplayacak
2 - 1 # 2'den 1 çıkartacak
2 * 2 # 2 ile 2'yi çarpacak
2 / 2 # 2'yi 2'ye bölecek

# yukarıdaki işlemlerin hepsi bir değişken ya da bir fonksiyon (örneğin print) içerisinde olmadığı için işlenip kaydedilmeden es geçilecek

print(2 + 2) # Burada print fonksiyonu içerisinde çalıştırdık, o yüzden print bize bunun çıktısını, yani 4'ü verecek.

sonuc = 2 + 2 # Burada işlemimizi "sonuc" adındaki bir değişkene kaydettik. sonuc çağırıldığında bize 4 verecek

print(sonuc) # Bize 4 verecek

print(2 * 4 + 3) # Bu bize önce 2 ile 4'ü çarpıp sonra 3 ile toplayacak

print(2 * (4 + 3)) # Bu sefer "4 + 3" parantez içinde olduğu için, onları önce işleyip sonra 2 ile çarpacak.