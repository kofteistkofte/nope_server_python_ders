"""
elimizde 3 değer var

ilk önce a 12mi diye
"""

a = 12
b = 42
c = "mustafa"

print(a == 12)
print(a == 24)
print(a == 'mahmut')
print(a == 12 and c == 'mustafa')

if True and True:
    print('Bu çalışır')

if True and False:
    print('Bu çalışmaz')

if True or True or True:
    print('Bu çalışır')

if True or False:
    print('Bu çalışır')

if False and False:
    print('Bu çalışmaz')
    
if not True:
    print('Bu çalışmaz')

if not False:
    print('Bu çalışır')
"""
if a == 13:
    do_someting()
elif a == 24:
    do_something_else()
elif a >= 5:
    do_more_stuff()
else:
    pass

if c == 'mahmut' and a == 12:
    do_something()
elif c == 'mustafa' and a == 24:
    do_something_else()
elif c == 'mustafa' and a != 24:
    do_more_stuff()
else:
    print('you fucked up')
"""

# deneme_fonksiyon adında bir fonksiyon yaratıldı
def deneme_fonksiyon():
    # fonksiyon içerisinde a ve b isimli değişkenler oluşturuldu.
    # bu değişkenler sadece bu fonksiyon içerisinde kullanılır.
    a = 'zimbo'
    b = 'bio'
    # burada başka bir fonksiyonu çalıştırıp ona işlem yaptırıyoruz.
    # Ama ardından fonksiyonumuz çalışmaya devam ediyor
    print(a)
    # burada a ve b değişkenlerini kullanarak yeni bir değişken oluşturduk
    c = a + " " + b
    # burada fonksiyonumuzun çıktı olarak c değerini vermesini istedik.
    # bunun anlamı deneme_fonksiyon() şeklinde çağırılınca "zimbo bio" diye bir çıktı verecek.
    # ama bununla bir işlem yapılmazsa sadece hafada kalır
    return c

deneme_fonksiyon()

cikti = deneme_fonksiyon()
print(cikti)

print(deneme_fonksiyon())


def dongu_fonksiyon(i):
    i += 1 # i = i + 1
    print(i)
    if i == 10:
        return 'True'
    return dongu_fonksiyon(i)
    
print(dongu_fonksiyon(0))

"""
dosya
-a = 12
-print(a) # 12
-fonksiyon
--print(a) # 12
--a = "mustafa"
--print(a) # mustafa
-print(a) # 12

- None
-- None
--- None
---- None
----- None
------ True
"""